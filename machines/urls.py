from django.urls import *
from .views import *
from django.views.generic import *

urlpatterns = [
    path('laptop/', LaptopList.as_view(), name='laptop_list'),
    path("add_item/", addMachine.as_view(), name="add_machine"),
    path("other/", OtherList.as_view(), name="other_list"),
    path("", RedirectView.as_view(url="laptop/"))
]
