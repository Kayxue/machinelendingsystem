from django.shortcuts import render
from django.urls import reverse
from django.views.generic import *
from .models import *
from .forms import *
import random

# Create your views here.


class LaptopList(ListView):
    model = MachineItem
    template_name = "machines/machine_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_title'] = "筆電列表"
        context['type_of_machine'] = "筆電"
        return context


class OtherList(ListView):
    model = MachineItem
    template_name = "machines/machine_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_title'] = "其他設備列表"
        context['type_of_machine'] = "其他設備"
        return context


class addMachine(CreateView):
    model = MachineItem
    #fields = ["brand", "boughtdate", "details", "note","type_of_machine"]
    form_class = addMachineForm
    template_name = "machines/machineitem_form.html"

    def get_success_url(self):
        return reverse("laptop_list")
