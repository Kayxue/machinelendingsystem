from django import forms
from .models import *


class addMachineForm(forms.ModelForm):
    class Meta:
        model = MachineItem
        fields = ["type_of_machine", "brand", "machine_number",
                  "item_number", "boughtdate",  "details", "note"]
        widgets = {
            "type_of_machine": forms.Select(
                attrs={
                    "class": "mt-2 mb-2"
                }
            ),
            "brand": forms.TextInput(
                attrs={
                    "class": "form-control mt-2 mb-2"
                }
            ),
            "machine_number": forms.TextInput(
                attrs={
                    "class": "form-control mt-2 mb-2"
                }
            ),
            "item_number": forms.TextInput(
                attrs={
                    "class": "form-control mt-2 mb-2"
                }
            ),
            "boughtdate": forms.TextInput(
                attrs={
                    "class": "datepicker"
                }
            ),
            "details": forms.Textarea(
                attrs={
                    "class": "form-control mt-2 mb-2"
                }
            ),
            "note": forms.Textarea(
                attrs={
                    "class": "form-control mt-2 mb-2"
                }
            )
        }
