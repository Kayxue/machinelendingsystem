from django.db import models

# Create your models here.


class MachineItem(models.Model):
    machine_status = [
        (0, "使用中"),
        (1, "無人使用"),
        (2, "已報廢")
    ]

    machine_type = [
        (0, "筆電"),
        (1, "其他設備")
    ]

    type_of_machine = models.IntegerField(
        "設備類型", default=0, choices=machine_type)

    brand = models.CharField("設備型號", max_length=20)

    boughtdate = models.DateField("購買日期", auto_now=False)

    details = models.TextField("詳細規格")

    now_machine_status = models.IntegerField(
        "目前狀態", default=0, choices=machine_status)

    machine_number = models.CharField("設備編號", max_length=9)

    item_number = models.CharField("財產編號", max_length=17)

    note = models.TextField("備註")

    def __str__(self):
        return self.brand
