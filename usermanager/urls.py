from django.urls import *
from .views import *
from django.views.generic import *

urlpatterns = [
    path('', RedirectView.as_view(url="inschool/")),
    path('inschool/',InSchool.as_view(),name="inschool_list"),
    path('left/', Left.as_view(),name="left_list")
]