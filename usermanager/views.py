from django.shortcuts import render
from django.views.generic import *
from django.urls import *
from django.contrib.auth.models import *
from django.contrib.auth.mixins import *
from .models import *
from .forms import *

# Create your views here.

class InSchool(ListView):
    model = Teacher
    template_name = "userlist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "在職"
        return context


class Left(ListView):
    model = Teacher
    template_name = "userlist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "已離職"
        return context


class addTeacher(CreateView):
    template_name = "addteacher.html"
    model = Teacher
    form_class=addTeacherForm

    def get_success_url(self):
        return reverse("inschool_list")
