from django.db import models

# Create your models here.


class Teacher(models.Model):
    stay = [
        (0, "在職"),
        (1, "已離職")
    ]
    name = models.CharField("姓名", max_length=4)

    stay_choose = models.IntegerField(
        "在職狀態", default=0, choices=stay
    )
