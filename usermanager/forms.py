from django import forms
from .models import *

class addTeacherForm(forms.ModelForm):
    class Meta:
        model=Teacher
        fields=["name","stay_choose"]
        widgets={
            "name":forms.TextInput(
                attrs={
                    "class":"form-control mt-2 mb-2" 
                }
            ),
            "stay_choose":forms.Select(
                attrs={
                    "class":"form-control mt-2 mb-2"    
                }
            )
        }